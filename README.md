## Run as nginx user

### Notes
When running nginx container **as nginx user**, it generates the following error:

```
2021/02/04 20:26:21 [warn] 1#1: the "user" directive makes sense only if the master process runs with super-user privileges, ignored in /etc/nginx/nginx.conf:2
nginx: [warn] the "user" directive makes sense only if the master process runs with super-user privileges, ignored in /etc/nginx/nginx.conf:2
2021/02/04 20:26:21 [emerg] 1#1: mkdir() "/var/cache/nginx/client_temp" failed (13: Permission denied)
nginx: [emerg] mkdir() "/var/cache/nginx/client_temp" failed (13: Permission denied)

```

### Steps to run the container

```
[ ]$ podman build -t nginx-perm .
STEP 1: FROM docker.io/library/nginx:1.19.6
STEP 2: COPY ./conf/self-signed.conf /etc/nginx/snippets/self-signed.conf
--> Using cache 62adb2e45e2b10cfe12a6758a54c50da88cf3655f131bee8f4fd7b4ea70cee35
--> 62adb2e45e2
STEP 3: COPY ./conf/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
--> Using cache 8c31fc2f78b9db6865968b8dbef48e9fad242129b0a2bbc4b520eb1e1c564c95
--> 8c31fc2f78b
STEP 4: RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
--> Using cache 62079c5366a4559d23cb6d55a4d795151add4474f064644c9c06d3316ae2700b
--> 62079c5366a
STEP 5: RUN echo '127.0.0.1 local-server-02' >> /etc/hosts
--> Using cache efff60afcf1b5417ab6ec943f970ddbcd49bd374c7305befda4eab5fff431a60
--> efff60afcf1
STEP 6: COPY ./conf/default.conf /etc/nginx/conf.d
--> Using cache 4c710a0c13f83add22c9861ffa6d6a6e47d42c0d933d4402916861d6493e785b
--> 4c710a0c13f
STEP 7: RUN mkdir -p /home/nginx/ssl
--> Using cache 4268595ae2a730030a1b337123f2c40da41920d9d11383db9d910a9010368189
--> 4268595ae2a
STEP 8: RUN chown -R nginx:nginx /home/nginx/ssl
--> Using cache 8198eb24d6a6436fe8745f6171bc4c2f057d4cced9f4929a02da21390ce5b9c9
--> 8198eb24d6a
STEP 9: USER nginx 
--> Using cache 136475aea4adbeea8eba474cc76a3b6072c069ac8cd90b441fab13e721cb815c
--> 136475aea4a
STEP 10: RUN openssl dhparam -out /home/nginx/ssl/dhparam.pem 2048
--> Using cache c50ded6d4d4425f09421280390f4e40deb8062da34ac27d5e57ac6d47b32fc6e
--> c50ded6d4d4
STEP 11: RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /home/nginx/ssl/nginx.key -out /home/nginx/ssl/nginx.crt -subj '/CN=www.mydom.com/O=My Company Name LTD./C=TN'
--> Using cache d3955ad7fa37fdd7acd980f88397972b322b7e06bf94cd77eff0e358097855ca
STEP 12: COMMIT nginx-perm
--> d3955ad7fa3
d3955ad7fa37fdd7acd980f88397972b322b7e06bf94cd77eff0e358097855ca
[ ]$ 
[ ]$ podman run --net=host --rm  nginx-perm
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: can not modify /etc/nginx/conf.d/default.conf (read-only file system?)
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2021/02/04 20:26:21 [warn] 1#1: the "user" directive makes sense only if the master process runs with super-user privileges, ignored in /etc/nginx/nginx.conf:2
nginx: [warn] the "user" directive makes sense only if the master process runs with super-user privileges, ignored in /etc/nginx/nginx.conf:2
2021/02/04 20:26:21 [emerg] 1#1: mkdir() "/var/cache/nginx/client_temp" failed (13: Permission denied)
nginx: [emerg] mkdir() "/var/cache/nginx/client_temp" failed (13: Permission denied)
```

## Run as root user
### Notes
When running the container **as root user** it works without problem

### Steps to run the container
```
[ ]$ echo USER root >> Dockerfile 
[ ]$
[ ]$ podman build -t nginx-perm .
STEP 1: FROM docker.io/library/nginx:1.19.6
STEP 2: COPY ./conf/self-signed.conf /etc/nginx/snippets/self-signed.conf
--> Using cache 62adb2e45e2b10cfe12a6758a54c50da88cf3655f131bee8f4fd7b4ea70cee35
--> 62adb2e45e2
STEP 3: COPY ./conf/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
--> Using cache 8c31fc2f78b9db6865968b8dbef48e9fad242129b0a2bbc4b520eb1e1c564c95
--> 8c31fc2f78b
STEP 4: RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
--> Using cache 62079c5366a4559d23cb6d55a4d795151add4474f064644c9c06d3316ae2700b
--> 62079c5366a
STEP 5: RUN echo '127.0.0.1 local-server-02' >> /etc/hosts
--> Using cache efff60afcf1b5417ab6ec943f970ddbcd49bd374c7305befda4eab5fff431a60
--> efff60afcf1
STEP 6: COPY ./conf/default.conf /etc/nginx/conf.d
--> Using cache 4c710a0c13f83add22c9861ffa6d6a6e47d42c0d933d4402916861d6493e785b
--> 4c710a0c13f
STEP 7: RUN mkdir -p /home/nginx/ssl
--> Using cache 4268595ae2a730030a1b337123f2c40da41920d9d11383db9d910a9010368189
--> 4268595ae2a
STEP 8: RUN chown -R nginx:nginx /home/nginx/ssl
--> Using cache 8198eb24d6a6436fe8745f6171bc4c2f057d4cced9f4929a02da21390ce5b9c9
--> 8198eb24d6a
STEP 9: USER nginx 
--> Using cache 136475aea4adbeea8eba474cc76a3b6072c069ac8cd90b441fab13e721cb815c
--> 136475aea4a
STEP 10: RUN openssl dhparam -out /home/nginx/ssl/dhparam.pem 2048
--> Using cache c50ded6d4d4425f09421280390f4e40deb8062da34ac27d5e57ac6d47b32fc6e
--> c50ded6d4d4
STEP 11: RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /home/nginx/ssl/nginx.key -out /home/nginx/ssl/nginx.crt -subj '/CN=www.mydom.com/O=My Company Name LTD./C=TN'
--> Using cache d3955ad7fa37fdd7acd980f88397972b322b7e06bf94cd77eff0e358097855ca
--> d3955ad7fa3
STEP 12: USER root
--> Using cache 9e0ab408a7c3dcbb488ee6ab937b7baaa865bb17564ae7d0b50e07f30365b1da
STEP 13: COMMIT nginx-perm
--> 9e0ab408a7c
9e0ab408a7c3dcbb488ee6ab937b7baaa865bb17564ae7d0b50e07f30365b1da
[ ]$
[ ]$ podman run --net=host --rm  nginx-perm
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf differs from the packaged version
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
```

