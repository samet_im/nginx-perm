FROM docker.io/library/nginx:1.19.6

COPY ./conf/self-signed.conf /etc/nginx/snippets/self-signed.conf
COPY ./conf/ssl-params.conf /etc/nginx/snippets/ssl-params.conf

RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
RUN echo '127.0.0.1 local-server-02' >> /etc/hosts

COPY ./conf/default.conf /etc/nginx/conf.d

RUN mkdir -p /home/nginx/ssl
RUN chown -R nginx:nginx /home/nginx/ssl


USER nginx 

RUN openssl dhparam -out /home/nginx/ssl/dhparam.pem 2048
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /home/nginx/ssl/nginx.key -out /home/nginx/ssl/nginx.crt -subj '/CN=www.mydom.com/O=My Company Name LTD./C=TN'

